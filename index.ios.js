import App from './App/Containers/App'
import { AppRegistry } from 'react-native'

AppRegistry.registerComponent('WhereIsEat', () => App)
