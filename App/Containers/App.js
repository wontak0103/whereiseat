import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'

const { width, height } = Dimensions.get('window');

const SCREEN_WIDTH = width;
const SCREEN_HEIGHT = height;
const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class App extends Component {
  watchID: ?number = null;

  constructor(props) {
    super(props);

    this.state = {
      initialRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },

      lastRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },

      myPosition: {
        latitude: 0,
        longitude: 0,
      },
    }
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        var latitude = parseFloat(position.coords.latitude);
        var longitude = parseFloat(position.coords.longitude);

        var initialRegion = {
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };

        this.setState({initialRegion});
        this.setState({lastRegion: initialRegion});
      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );

    this.watchID = navigator.geolocation.watchPosition((position) => {
      var latitude = parseFloat(position.coords.latitude);
      var longitude = parseFloat(position.coords.longitude);

      var lastRegion = {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      };

      this.setState({lastRegion});
      this.setState({myPosition: lastRegion});
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  search() {

  }

  render() {
    return (
      <View style={styles.container}>

        <MapView style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={this.state.initialRegion}
          region={this.state.lastRegion}>

          <MapView.Marker
            coordinate={this.state.myPosition}>

              <View style={styles.my_marker_radius}>
                <View style={styles.my_marker}/>
              </View>

            </MapView.Marker>

        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  my_marker: {
    width: 20,
    height: 20,
    overflow: 'hidden',
    borderWidth: 3,
    borderRadius: 20 / 2,
    borderColor: 'white',
    backgroundColor: 'rgba(0, 122, 255, 1)',
  },

  my_marker_radius: {
    width: 50,
    height: 50,
    overflow: 'hidden',
    borderWidth: 1,
    borderRadius: 50 / 2,
    borderColor: 'rgba(0, 122, 255, 0.3)',
    backgroundColor: 'rgba(0, 122, 255, 0.1)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
